export interface IMetaFieldOptions {
  name?: string;
  type?: 'string' | 'number' | 'date' | 'boolean' | 'text' | 'url';
  widget?: string;
  widgetOptions?: {[optionsName: string]: any};
  visible?: boolean;
}
export class EmergencyMeta {
  fields: {[fieldName: string]: IMetaFieldOptions};
  display: string[];
  filterFields: string[];
}


export class Emergency {
  name: string;
  descript: string;
  region: string;
  lon: number;
  geo: string;
  lat: number;
  num: string;
  date: string;
  type: 'аварийная ситуация' | 'спорная ситуация';
  id: number;

  // may be placed inside each feature or in the parent FeatureCollection object
  meta?: EmergencyMeta;
}
