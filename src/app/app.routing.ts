// https://angular.io/docs/ts/latest/guide/router.html
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HomeComponent} from './components/home/home.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {MapComponent} from './components/map/map.component';


const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/map',
    pathMatch: 'full',
    // component: HomeComponent,
    // canActivate: [AuthGuard]
  },
  {
    path: 'map',
    component: MapComponent,
    data: {serviceName: 'leaflet'},
    pathMatch: 'full',
    // canActivate: [AuthGuard]
  },
  {
    path: '**',
    component: NotFoundComponent,
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}







