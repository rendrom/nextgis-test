import {Injectable} from '@angular/core';
import {LeafletService} from './leaflet.service';
import {MapboxglService} from './mapboxgl.service';
import {MapService} from './map.service';

export type AllowedMapServices = 'leaflet' | 'mapboxgl';

@Injectable({
  providedIn: 'root'
})
export class UniversalMapService {

  public mapService: AllowedMapServices = 'leaflet';

  constructor(
    private _leafletService: LeafletService,
    private _mapboxglService: MapboxglService
  ) {}

  public get instance(): MapService {
    if (this.mapService === 'leaflet') {
      return this._leafletService;
    } else {
      return this._mapboxglService;
    }
  }
}
