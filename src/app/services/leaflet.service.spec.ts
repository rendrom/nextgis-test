import { TestBed, inject } from '@angular/core/testing';

import { LeafletService } from './leaflet.service';

describe('LeafletService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LeafletService]
    });
  });

  it('should be created', inject([LeafletService], (service: LeafletService) => {
    expect(service).toBeTruthy();
  }));
});
