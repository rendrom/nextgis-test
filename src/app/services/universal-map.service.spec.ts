import { TestBed, inject } from '@angular/core/testing';

import { UniversalMapService } from './universal-map.service';

describe('UniversalMapService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UniversalMapService]
    });
  });

  it('should be created', inject([UniversalMapService], (service: UniversalMapService) => {
    expect(service).toBeTruthy();
  }));
});
