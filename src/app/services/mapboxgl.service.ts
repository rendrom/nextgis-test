import {Injectable, EventEmitter} from '@angular/core';
import {MapService, IAddGeoJSONLayerOptions} from './map.service';
import {FeatureCollection} from 'geojson';

import {Map, MapboxOptions} from 'mapbox-gl';
import {Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MapboxglService implements MapService {

  options: mapboxgl.MapboxOptions = {};

  selectedLayerChange = new EventEmitter<string>();

  geojsonStyleOptions: IAddGeoJSONLayerOptions = {
    circle: {
      style: {
        'circle-radius': 6
      }
    }
  };

  private map: Map;
  private mapLoaded: boolean;
  private mapLoadedEvent = new EventEmitter();
  private mapLoadedSubscribe: Subscription;
  private mapLoadedSubscribeForGeoJsonlayer: Subscription;

  constructor() {}

  destroy() {
    this.map.remove();
    this.map = null;
    this.mapLoaded = false;

    [
      this.mapLoadedSubscribe,
      this.mapLoadedSubscribeForGeoJsonlayer
    ].forEach((x) => {
      if (x) {
        x.unsubscribe();
      }
    });

  }

  create(mapId: string, options?: MapboxOptions) {
    this.options = Object.assign({
      container: mapId,
      style: {
        version: 8,
        name: 'Empty style',
        sources: {},
        layers: [],
      }
    }, this.options, options);

    // TODO: set correct maxBounds
    delete this.options.maxBounds;

    // this.map = new Map(this.options);
    this.map = new Map(this.options);
    this.map.on('load', () => {
      this.mapLoaded = true;
      this.mapLoadedEvent.emit();
    });
  }

  clearLayers() {}

  clearLayer(layerName: string) {}

  addTileLayer(layerName: string, url: string, params?: mapboxgl.RasterSource) {
    const addLayer = () => this._addTilelayer(layerName, url, params);
    if (!this.mapLoaded) {
      if (this.mapLoadedSubscribe) {
        this.mapLoadedSubscribe.unsubscribe();
      }
      this.mapLoadedSubscribe = this.mapLoadedEvent.subscribe(addLayer);
    } else {
      addLayer();
    }
  }

  addControl(name: string) {}

  addGeoJsonLayer(name: string, geojson: FeatureCollection, options: IAddGeoJSONLayerOptions = {}) {
    const addLayer = () => this._addGeoJsonLayer(name, geojson, options);
    if (!this.mapLoaded) {
      if (this.mapLoadedSubscribeForGeoJsonlayer) {
        this.mapLoadedSubscribeForGeoJsonlayer.unsubscribe();
      }
      this.mapLoadedSubscribeForGeoJsonlayer = this.mapLoadedEvent.subscribe(addLayer);
    } else {
      addLayer();
    }

  }

  setLayerFilter(layerName: string, filter: (feature) => boolean) {}

  _addTilelayer(layerName: string, url: string, params?: {subdomains?: string, attribution?: string, tileSize?: number}) {
    // this.map.addSource(layerName, params);

    let tiles;
    if (params && params.subdomains) {
      tiles = params.subdomains.split('').map((x) => {
        const subUrl = url.replace('{s}', x);
        return subUrl;
      });
    } else {
      tiles = [url];
    }

    this.map.addLayer({
      id: layerName,
      type: 'raster',
      source: {
        'type': 'raster',
        // point to our third-party tiles. Note that some examples
        // show a "url" property. This only applies to tilesets with
        // corresponding TileJSON (such as mapbox tiles).
        'tiles': tiles,
        'tileSize': params && params.tileSize || 256
      }
    });
  }

  _addGeoJsonLayer(name: string, geojson: FeatureCollection, options: IAddGeoJSONLayerOptions = {}) {

    const addLayerOptions: mapboxgl.Layer = {
      'id': 'route',
      'type': 'circle',
      'source': {
        'type': 'geojson',
        'data': geojson
      }
    };

    if (options && options.circle) {
      const featureOpt = this.geojsonStyleOptions.circle.style;
      const setStyle = options.circle && options.circle.setStyle;
      if (setStyle) {
        // const funStyle = setStyle(feature);
        // if (funStyle) {
        //   Object.assign(featureOpt, funStyle);
        // }
      }
      addLayerOptions.paint = {
        'circle-radius': 6,
        'circle-color': [
          'match',
          ['get', 'type'],
          'аварийная ситуация', '#ff0000',
          'спорная ситуация', '#8a0ffc',
          /* other */ '#ccc'
        ]
      };
    }

    this.map.addLayer(addLayerOptions);
  }
}
