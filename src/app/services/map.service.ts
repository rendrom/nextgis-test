import {Injectable, EventEmitter} from '@angular/core';
import {Feature, FeatureCollection} from 'geojson';

export interface IAddGeoJSONLayerOptions {
  circle?: {
    setStyle?: (feature: Feature) => any;
    setSelectStyle?: (feature: Feature) => any;
    style?: any; // L.CircleMarkerOptions;
  };
  onClick?: (e: {target: {feature: Feature}}) => void;
  filter?: (feature: Feature) => boolean;
}

export interface IMapOptions {
  id: string;
}

@Injectable({
  providedIn: 'root'
})
export abstract class MapService {

  options: any = {};

  selectedLayerChange = new EventEmitter<string>();

  geojsonStyleOptions = {
  };


  constructor() {  }

  destroy() {}

  create(mapId: string, options?: any) {
    // ignore
  }

  clearLayers() {}

  clearLayer(layerName: string) {}

  addTileLayer(layerName: string, url: string, params?: any) {}

  addControl(name: string) {}

  addGeoJsonLayer(name: string, geojson: FeatureCollection, options: IAddGeoJSONLayerOptions = {}) {}

  setLayerFilter(layerName: string, filter: (feature) => boolean) {}

}
