import { TestBed, inject } from '@angular/core/testing';

import { MapboxglService } from './mapboxgl.service';

describe('MapboxglService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MapboxglService]
    });
  });

  it('should be created', inject([MapboxglService], (service: MapboxglService) => {
    expect(service).toBeTruthy();
  }));
});
