import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Emergency, EmergencyMeta} from '../classes/emergency';
import EMERGENCY from '../classes/data.json';
import META from '../classes/meta.json';
import {FeatureCollection, Point} from '../../../node_modules/@types/geojson';

export interface EmergencyFeatureCollection extends FeatureCollection<Point, Emergency> {
  meta: EmergencyMeta;
}

@Injectable({
  providedIn: 'root'
})
export class EmergenciesService {

  url = 'http://nextgis.ru/share/data.geojson';

  constructor(private http: HttpClient) {
  }

  get(): Observable<EmergencyFeatureCollection> {
    const emergency: EmergencyFeatureCollection = EMERGENCY;
    emergency.meta = META;
    return of(EMERGENCY);
    // return this.http.get<Emergency[]>(this.url)
    //   .pipe(catchError(this.handleError));
  }

  private handleError(error: any) {
    let errMsg: string;
    errMsg = 'Server error occurred please try again.';

    return Observable.throw(errMsg);
  }

}
