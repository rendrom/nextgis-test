import { TestBed, inject } from '@angular/core/testing';

import { EmergenciesService } from './emergencies.service';

describe('EmergenciesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmergenciesService]
    });
  });

  it('should be created', inject([EmergenciesService], (service: EmergenciesService) => {
    expect(service).toBeTruthy();
  }));
});
