import {Injectable, EventEmitter} from '@angular/core';
import {MapService, IAddGeoJSONLayerOptions} from './map.service';
import {Map, TileLayer, Control, GeoJSON, CircleMarker, GeoJSONOptions} from 'leaflet';
import {Feature, Point, FeatureCollection} from 'geojson';


@Injectable({
  providedIn: 'root'
})
export class LeafletService implements MapService {

  static Controls = {
    Zoom: Control.Zoom
  };

  static ControlsOptions = {
    Zoom: {position: 'topleft'}
  };

  options: L.MapOptions = {
    dragging: true,
    touchZoom: true,
    scrollWheelZoom: true,
    doubleClickZoom: true,
    boxZoom: true,
    trackResize: true,
    closePopupOnClick: true,
    zoomControl: false,
    attributionControl: true,
    zoomAnimation: false,
    fadeAnimation: false,
  };

  selectedLayerChange = new EventEmitter<string>();

  geojsonStyleOptions: IAddGeoJSONLayerOptions = {
    circle: {
      style: {
        radius: 6
      }
    }
  };

  private map: L.Map;

  private _tileLayers: {[layerName: string]: TileLayer} = {};
  private _geojsonLayers: {
    [layerName: string]: {
      layer: GeoJSON<any>,
      name: string,
      options: IAddGeoJSONLayerOptions
      geojson: FeatureCollection
    }
  } = {};

  private selectedLayer: {
    layer: CircleMarker,
    name: string,
    options: IAddGeoJSONLayerOptions
  };

  constructor() {
  }

  destroy() {
    // this.selectedLayerChange = null;
    this._removeSelectedLayer();
    this.clearLayers();
    this.map.remove();
    this.map = null;

    // this.geojsonStyleOptions = null;
  }

  clearLayers() {
    Object.keys(this._geojsonLayers).forEach((k) => {
      this.clearLayer(k);
    });
  }

  clearLayer(layerName: string) {
    const data = this._geojsonLayers[layerName];
    if (data) {
      data.layer.off();
      data.layer.removeFrom(this.map);
      delete this._geojsonLayers[layerName];
    }
  }

  create(mapId: string, options: L.MapOptions = {}) {
    this.options = Object.assign({}, this.options, options);
    this.map = new Map(mapId, this.options);
  }

  addControl(controlDef: string | [string, {[param: string]: any}]) {
    if (typeof controlDef === 'string') {
      const C = LeafletService.Controls[controlDef];
      if (C) {
        const control = new C(LeafletService.ControlsOptions[controlDef] || {}) as Control;
        control.addTo(this.map);
      }
    }
    // TODO: control with options
  }

  addTileLayer(name: string, url: string, params: L.TileLayerOptions = {}) {
    const layer = new TileLayer(url, params);
    this._tileLayers[name] = layer;
    this.map.addLayer(layer);
    return layer;
  }


  setLayerFilter(layerName: string, filter: (feature) => boolean) {
    this._removeSelectedLayer();

    const filtered = this._geojsonLayers[layerName];
    const layer = filtered && filtered.layer;
    if (layer) {
      layer.removeFrom(this.map);

      const options = Object.assign({}, filtered.options);

      options.filter = filter;

      const filteredLayer = this._createGeoJSONLayer(filtered.geojson, options);

      filteredLayer.addTo(this.map);

      filtered.layer = filteredLayer;

    }
  }

  addGeoJsonLayer(name: string, geojson: FeatureCollection, options: IAddGeoJSONLayerOptions = {}) {
    const layer = this._createGeoJSONLayer(geojson, options);

    this._geojsonLayers[name] = {layer, name, options, geojson};
    this.map.addLayer(layer);
    return layer;
  }

  _createGeoJSONLayer(geojson: FeatureCollection, options: IAddGeoJSONLayerOptions): GeoJSON {
    const createLayerOptions: GeoJSONOptions = {
      pointToLayer: (feature, latlng) => {
        const featureOpt = this.geojsonStyleOptions.circle.style;
        const setStyle = options.circle && options.circle.setStyle;
        if (setStyle) {
          const funStyle = setStyle(feature);
          if (funStyle) {
            Object.assign(featureOpt, funStyle);
          }
        }
        return new CircleMarker(latlng, featureOpt);
      },
      onEachFeature: (feature, l) => {
        const onClick = options.onClick;
        l.on('click', (e: {target: {feature: Feature<Point>}}) => {
          if (onClick) {
            onClick(e);
          }
          this._setSelectedLayer(name, l, options);
        });

      }
    };
    if (options.filter) {
      createLayerOptions.filter = options.filter;
    }
    return new GeoJSON(geojson, createLayerOptions);
  }

  _setSelectedLayer(name, layer, options) {
    const curentSelectedlayer = this.selectedLayer && this.selectedLayer.layer;
    this._removeSelectedLayer(true);
    const isSame = curentSelectedlayer === layer;
    if (!isSame) {

      this.selectedLayer = {layer, name, options};

      const setStyle = this.selectedLayer.options.circle.setSelectStyle;
      if (setStyle) {
        this.selectedLayer.layer.setStyle(setStyle(this.selectedLayer.layer.feature));
      }


    }

    this.selectedLayerChange.emit(!isSame && layer.feature);
  }

  _removeSelectedLayer(silent?: boolean) {
    if (this.selectedLayer) {
      const setStyle = this.selectedLayer.options.circle.setStyle;
      if (setStyle) {
        this.selectedLayer.layer.setStyle(setStyle(this.selectedLayer.layer.feature));
      }
      this.selectedLayer = null;
    }
    if (this.selectedLayerChange && !silent) {
      this.selectedLayerChange.emit();
    }
  }

}
