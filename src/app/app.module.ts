import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './components/app.component';

import {
  MatButtonModule,
  MatIconModule,
  MatToolbarModule,
  MatMenuModule,
  MatCardModule,
  MatFormFieldModule,
  // MatProgressSpinnerModule,
  // MatInputModule,
  MatSidenavModule,
  MatAutocompleteModule,
  MatInputModule,
  MatButtonToggleModule
} from '@angular/material';
import {AppRoutingModule} from './app.routing';
import {HomeComponent} from './components/home/home.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {MapComponent} from './components/map/map.component';
import {LeafletService} from './services/leaflet.service';
import {EmergenciesService} from './services/emergencies.service';
import {FeatureInfoComponent} from './components/feature-info/feature-info.component';
import { FeaturePropertyComponent } from './components/feature-property/feature-property.component';
import { FeaturesFilterComponent } from './components/features-filter/features-filter.component';


@NgModule({
  declarations: [
    AppComponent,

    HomeComponent,
    NotFoundComponent,
    MapComponent,
    FeatureInfoComponent,
    FeaturePropertyComponent,
    FeaturesFilterComponent
  ],
  imports: [

    FormsModule,
    ReactiveFormsModule,

    HttpClientModule,

    BrowserModule,
    BrowserAnimationsModule,

    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatCardModule,
    MatSidenavModule,
    MatAutocompleteModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonToggleModule,

    AppRoutingModule
  ],
  providers: [
    HttpClient,
    LeafletService,
    EmergenciesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
