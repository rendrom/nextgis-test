import {Component, OnInit, Input} from '@angular/core';
import {IMetaFieldOptions, Emergency} from '../../classes/emergency';

import {Util} from 'leaflet';

@Component({
  selector: 'app-feature-property',
  templateUrl: './feature-property.component.html',
  styleUrls: ['./feature-property.component.css']
})
export class FeaturePropertyComponent implements OnInit {

  @Input() properties: Emergency;
  @Input() property: string;
  @Input() options: IMetaFieldOptions = {type: 'string'};

  constructor() {
  }

  ngOnInit() {
  }

  onDetailClick(detailOptions: {linkTemplate: string}) {
    const linkTemplate = detailOptions && detailOptions.linkTemplate;

    if (linkTemplate) {
      const url = Util.template(linkTemplate, this.properties);
      if (url) {
        window.open(url, '_blank');
      }
    }
  }

}
