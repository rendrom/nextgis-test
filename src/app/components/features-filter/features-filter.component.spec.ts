import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturesFilterComponent } from './features-filter.component';

describe('FeaturesFilterComponent', () => {
  let component: FeaturesFilterComponent;
  let fixture: ComponentFixture<FeaturesFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturesFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturesFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
