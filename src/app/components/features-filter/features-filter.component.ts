import {Component, OnInit, Input, Output, EventEmitter, OnDestroy, OnChanges} from '@angular/core';
import {Emergency, EmergencyMeta} from '../../classes/emergency';
import {FormControl} from '@angular/forms';
import {Observable, Subscriber, Subscription} from 'rxjs';
import {map, startWith} from 'rxjs/operators';


@Component({
  selector: 'app-features-filter',
  templateUrl: './features-filter.component.html',
  styleUrls: ['./features-filter.component.css']
})
export class FeaturesFilterComponent implements OnInit, OnDestroy, OnChanges {

  @Input() emergencies: Emergency[];
  @Input() meta?: EmergencyMeta;
  @Input() filterField = 'name';
  @Output() filterChange = new EventEmitter<string>();

  filterFieldName = 'Название компании';

  form = new FormControl();

  options = [];

  filteredOptions: Observable<Emergency[]>;

  onlyUnique(value, index, self) {
    return self.map((x) => x[this.filterField]).indexOf(value[this.filterField]) === index;
  }

  ngOnInit() {
    this.options = this.emergencies.filter(this.onlyUnique.bind(this));
  }

  ngOnChanges() {

    this.form.reset();

    const metaField = this.meta && this.meta.fields && this.meta.fields[this.filterField];
    if (metaField) {
      this.filterFieldName = metaField.name;
    }

    this.filteredOptions = this.form.valueChanges
      .pipe(
        startWith<string | Emergency>(''),
        map((value) => typeof value === 'string' ? value : value[this.filterField]),
        map((value) => {

          this.filter(value);
          this.onFilterChange(value);
          const displayOptions = this.options.slice();
          if (value) {
            const index = displayOptions.map((x) => x[this.filterField]).indexOf(value);
            if (index !== -1) {
              displayOptions.splice(index, 1);
            }
          }
          return displayOptions;
        }
        )
      );
  }

  ngOnDestroy() {

  }

  filter(name: string): Emergency[] {
    return this.options.filter(option => {
      const o = option[this.filterField];
      return o.toLowerCase().indexOf(name.toLowerCase()) === 0;
    });
  }

  displayFn(emergency?: string): string | undefined {
    return emergency ? emergency : undefined;
  }

  onFilterChange(value: string) {
      this.filterChange.emit(value);
  }

}
