import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import {Emergency, EmergencyMeta} from '../../classes/emergency';
import {Point, Feature} from 'geojson';

@Component({
  selector: 'app-feature-info',
  templateUrl: './feature-info.component.html',
  styleUrls: ['./feature-info.component.css']
})
export class FeatureInfoComponent implements OnInit, OnDestroy {

  @Input() feature: Feature<Point, Emergency>;
  @Input() meta?: EmergencyMeta;

  constructor() { }

  ngOnInit() {
  }

  ngOnDestroy() {

  }

}
