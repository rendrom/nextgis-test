import {Component, OnDestroy, AfterViewInit, ChangeDetectorRef, OnInit} from '@angular/core';
import {LeafletService} from '../../services/leaflet.service';
import {EmergenciesService, EmergencyFeatureCollection} from '../../services/emergencies.service';
import {Subscription} from 'rxjs';
import {Point, Feature} from '../../../../node_modules/@types/geojson';
import {Emergency, EmergencyMeta} from '../../classes/emergency';
import {UniversalMapService, AllowedMapServices} from '../../services/universal-map.service';
import {MapService, IAddGeoJSONLayerOptions} from '../../services/map.service';
import {ActivatedRoute} from '@angular/router';

// import 'leaflet/dist/images/marker-icon-2x.png';
// import 'leaflet/dist/images/marker-icon.png';
// import 'leaflet/dist/images/marker-shadow.png';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnDestroy, OnInit, AfterViewInit {

  emergencies: Emergency[];

  featuresMeta: EmergencyMeta;

  filterField: keyof Emergency = 'name';

  filterFields: Array<{value: keyof Emergency, name: string}>;

  filterValue: any;

  layerData: EmergencyFeatureCollection;

  mapService: AllowedMapServices;
  mapId = 'map';

  mapServiceName: AllowedMapServices = 'leaflet';

  mapServices: Array<{value: AllowedMapServices, name: string}> = [
    {value: 'leaflet', name: 'Leaflet'},
    {value: 'mapboxgl', name: 'MapboxGL'}
  ];

  mapOptions = {
    center: {lat: 60, lng: 90},
    zoom: 3,
    // maxZoom: 18,
    // minZoom: 3,
    // maxBounds: [[-90, -90], [90, 270]],
  };

  selectedFeature: Feature<Point, Emergency>;

  private map: MapService;

  private mapContainer: HTMLElement;

  private isViewInitialized = false;
  private selectedLayerChangeSubscribe: Subscription;

  private getEmergenciesSubscribe: Subscription;
  private subscribeRouter: Subscription;

  constructor(
    private route: ActivatedRoute,
    private universalMap: UniversalMapService,
    private emergenciesService: EmergenciesService,
    private cdRef: ChangeDetectorRef) {
    this.mapContainer = this._createMapContainer();
  }

  ngOnInit() {
    this.getEmergenciesSubscribe = this.emergenciesService.get().subscribe(this._onEmergenciesLoad.bind(this));
    this.subscribeRouter = this.route.data
      .subscribe((data: {serviceName: AllowedMapServices}) => this.setMapService(data.serviceName));
  }

  ngAfterViewInit() {
    this.isViewInitialized = true;
    this._updateMapComponent();
    this.cdRef.detectChanges();
  }

  ngOnDestroy() {
    if (this.getEmergenciesSubscribe) {
      this.getEmergenciesSubscribe.unsubscribe();
    }
    if (this.selectedLayerChangeSubscribe) {
      this.selectedLayerChangeSubscribe.unsubscribe();
    }
    if (this.subscribeRouter) {
      this.subscribeRouter.unsubscribe();
    }
    this._destroyMap();
  }

  setMapService(mapServiceName: AllowedMapServices) {
    this._destroyMap();
    this.mapServiceName = mapServiceName;
    this.universalMap.mapService = this.mapServiceName;
    this.map = this.universalMap.instance;
  }

  changeMapService(mapServiceName: AllowedMapServices) {
    this.setMapService(mapServiceName);
    this._updateMapComponent();
  }

  changeFilterField(name: keyof Emergency) {
    this.filterField = name;
  }

  filterChange(value?: any) {
    this.filterValue = value;
    this.map.setLayerFilter('emergency', (faeture) => this._layerFilter(faeture, value));
  }

  _layerFilter(feature, value: any): boolean {
    if (value) {
      return feature.properties[this.filterField] === value;
    }
    return true;
  }

  _destroyMap() {
    this._clearContainer();
    if (this.map) {
      this.map.destroy();
    }
  }

  _clearContainer() {
    const parent = this.mapContainer && this.mapContainer.parentNode;
    if (parent) {
      parent.removeChild(this.mapContainer);
    }
  }

  _updateMapComponent() {
    if (!this.isViewInitialized) {
      return;
    }
    // this.elem.nativeElement.appendChild(this.mapContainer);
    this.map.create(this.mapId, this.mapOptions);
    this.map.addTileLayer('osm', 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://osm.org/copyright" target="_blank">OpenStreetMap</a> contributors',
      subdomains: 'abc'
    });

    this.map.addControl('Zoom');

    if (this.map.selectedLayerChange) {
      this.selectedLayerChangeSubscribe = this.map.selectedLayerChange.subscribe(this._onSelectionFeatureChange.bind(this));
    }

    this._addGeoJsonLayer();

  }

  _createMapContainer() {
    const mapContainer = document.createElement('div');
    mapContainer.id = this.mapId;
    return mapContainer;
  }

  _setCircleStyle(feature: Feature<Point, Emergency>, alias?): L.CircleMarkerOptions {
    alias = alias || {
      'аварийная ситуация': '#ff0000',
      'спорная ситуация': '#8a0ffc'
    };
    const style: L.CircleMarkerOptions = {};
    const type = feature.properties.type;
    if (type) {
      const color: string = alias[type] || '#d3d3d3';
      style.color = color;
      style.fillColor = color;
      return style;
    }
  }

  _onSelectionFeatureChange(feature?) {
    this.selectedFeature = feature;
  }

  _onFeatureClick(e: {target: {feature: Feature<Point, Emergency>}}) {

  }

  _setFilterFields() {
    const filterFields = this.featuresMeta && this.featuresMeta.filterFields;

    if (filterFields) {
      const fieldsMeta = this.featuresMeta && this.featuresMeta.fields;

      this.filterFields = filterFields.map((x: keyof Emergency) => {
        return {name: (fieldsMeta && fieldsMeta[x] && fieldsMeta[x].name) || x, value: x};
      });
    }
  }

  _addGeoJsonLayer() {
    const addlayerOptions: IAddGeoJSONLayerOptions = {
      circle: {
        setStyle: this._setCircleStyle,
        setSelectStyle: (feature: Feature<Point, Emergency>) => this._setCircleStyle(feature, {
          'аварийная ситуация': '#960404',
          'спорная ситуация': '#371b51'
        }),

      },
      onClick: this._onFeatureClick.bind(this),
    };
    if (this.filterValue) {
      addlayerOptions.filter = (faeture) => this._layerFilter(faeture, this.filterValue);
    }
    this.map.addGeoJsonLayer('emergency', this.layerData, addlayerOptions);
  }

  _onEmergenciesLoad(data: EmergencyFeatureCollection) {

    this.layerData = data;

    this.featuresMeta = data.meta;

    this.emergencies = data.features.map((x) => x.properties);

    this._setFilterFields();
  }
}
