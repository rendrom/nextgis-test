# NextgisTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Installation

Run `git clone https://rendrom@bitbucket.org/rendrom/nextgis-test` to get last version of project.
Run `cd ./nextgis-test` to move in the project folder.
Run `npm i` for install all project dependencies.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory..
Run `npm run prod` for a production build

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
